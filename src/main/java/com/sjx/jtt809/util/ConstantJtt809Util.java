package com.sjx.jtt809.util;

import io.netty.channel.ChannelHandlerContext;

import java.util.HashMap;
import java.util.Map;

public class ConstantJtt809Util {

    /**
     * msgId在数据包中的位置
     * 计算方式：
     * Head_flag（1 byte） + MSG_LENGT（4 byte） + MSG_SN（4 byte）= 9
     */
    public static final int MSG_ID_INDEX_OF_PACKAGE = 9;

    /**
     * data_type在数据包中的位置
     * 计算方式：
     */
    public static final int DATA_TYPE_INDEX_OF_PACKAGE = 45;

    /**
     * 上下级平台地址关联集合
     */
    public static final Map<String, String> UP_DOWN_PLATFORM_LINK = new HashMap<String, String>(10);

    /**
     * 上级平台地址连接集合
     */
    public static final Map<String, ChannelHandlerContext> UP_PLATFORM = new HashMap<String, ChannelHandlerContext>(10);

    /**
     * 下级平台地址连接集合
     */
    public static final Map<String, ChannelHandlerContext> DOWN_PLATFORM = new HashMap<String, ChannelHandlerContext>(10);

    /**
     * 链路类型：主链路
     * 描述：主链路登录请求消息
     */
    public static final int UP_CONNECT_REQ = 0x1001;

    /**
     * 链路类型：主链路
     * 描述：主链路登录应答消息
     */
    public static final int UP_CONNECT_REP = 0x1002;

    /**
     * 链路类型：主链路
     * 描述：主链路连接保持请求消息
     */
    public static final int UP_LINKETEST_REQ = 0x1005;

    /**
     * 链路类型：主链路
     * 描述：主链路连接保持应答消息
     */
    public static final int UP_LINKTEST_RSP = 0x1006;

    /**
     * 链路类型：从链路
     * 描述：从链路连接保持请求消息
     */
    public static final int DOWN_LINKTEST_REQ = 0x9005;

    /**
     * 链路类型：从链路
     * 描述：从链路连接保持应答消息
     */
    public static final int DOWN_LINKTEST_RSP = 0x9006;

    /**
     * 链路类型：从链路
     * 描述：从链路连接请求消息
     */
    public static final int DOWN_CONNECT_REQ = 0x9001;

    /**
     * 链路类型：从链路
     * 描述：从链路连接应答消息
     */
    public static final int DOWN_CONNECT_RSP = 0x9002;

    /**
     * 链路类型：主链路
     * 描述：主链路动态信息交换消息
     */
    public static final int UP_EXG_MSG = 0x1200;

    /**
     * 链路类型：主链路
     * 描述：实时上传车辆定位信息
     */
    public static final int UP_EXG_MSG_REAL_LOCATION = 0x1202;

    /**
     * 链路类型：从链路
     * 描述：接收定位信息数量通知消息
     */
    public static final int DOWN_TOTAL_RECY_BACK_MSG = 0x9101;

}
