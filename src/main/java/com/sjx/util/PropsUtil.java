package com.sjx.util;

import cn.hutool.setting.dialect.Props;

/**
 * 配置文件类
 */
public class PropsUtil {

    /**
     * 定义config配置文件实例
     */
    private static Props config;

    /**
     * 单例模式获取config配置对象实例
     *
     * @return
     */
    public static Props getConfigInstance() {
        try {
            if (null == config) {
                synchronized (PropsUtil.class) {
                    if(null == config) {
                        config = new Props(ConstantUtil.PROPERTIES_CONFIG_NAME);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return config;
    }

}
