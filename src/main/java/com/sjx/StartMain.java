package com.sjx;

import com.sjx.jtt809.business.BusinessFactory;
import com.sjx.jtt809.manager.PrimaryLinkManagerJtt809;
import com.sjx.util.PropsUtil;
import com.sjx.util.ConstantUtil;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.StrUtil;

/**
 * netty启动器
 */
public class StartMain {

    /**
     * 绑定地址
     */
    private static final String JTT809_NETTY_SERVER_IP = PropsUtil.getConfigInstance().getStr(ConstantUtil.JTT809_NETTY_SERVER_IP);

    /**
     * 绑定端口
     */
    private static final String JTT809_NETTY_SERVER_PORT = PropsUtil.getConfigInstance().getStr(ConstantUtil.JTT809_NETTY_SERVER_PORT);

    public static void main(String[] args) {
        try {
            String[] ipArray = StrUtil.splitToArray(JTT809_NETTY_SERVER_IP, ',');
            int[] portArray = StrUtil.splitToInt(JTT809_NETTY_SERVER_PORT, ',');

            if (portArray != null && portArray != null) {
                if (ipArray.length == 1 && portArray.length == 1) {
                    // 单个ip与端口
                    ThreadUtil.execute(new PrimaryLinkManagerJtt809(JTT809_NETTY_SERVER_IP, Integer.parseInt(JTT809_NETTY_SERVER_PORT)));
                } else if (ipArray.length > 1 && portArray.length == 1) {
                    // 多个ip，单个端口
                    for (String ip : ipArray) {
                        ThreadUtil.execute(new PrimaryLinkManagerJtt809(ip, Integer.parseInt(JTT809_NETTY_SERVER_PORT)));
                        Thread.sleep(2000);
                    }
                } else if (ipArray.length == 1 && portArray.length > 1) {
                    // 单个ip，多个端口
                    for (int port : portArray) {
                        ThreadUtil.execute(new PrimaryLinkManagerJtt809(JTT809_NETTY_SERVER_IP, port));
                        Thread.sleep(2000);
                    }
                } else if (ipArray.length == portArray.length) {
                    // 多个ip，多个端口，且ip与端口一一对应
                    for (int i = 0; i < ipArray.length; i++) {
                        ThreadUtil.execute(new PrimaryLinkManagerJtt809(ipArray[i], portArray[i]));
                        Thread.sleep(2000);
                    }
                } else {
                    throw new Exception("存在多个IP地址与端口，但ip地址与端口个数不统一！");
                }
            }

            // 延迟5s
            Thread.sleep(5000);

            // 队列处理数据
            ThreadUtil.execute(new BusinessFactory());
            // ThreadUtil.execute(new BusinessFactory());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
