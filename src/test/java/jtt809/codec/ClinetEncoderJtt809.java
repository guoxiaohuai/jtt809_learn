package jtt809.codec;

import com.sjx.jtt809.pojo.BasePackage;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * jtt809编码类
 */
public class ClinetEncoderJtt809 extends MessageToByteEncoder<BasePackage> {

    private static final Log logger = LogFactory.get();

    protected void encode(ChannelHandlerContext ctx, BasePackage basePackage, ByteBuf out) throws Exception {
        ByteBuf sendToMsg = basePackage.encode();
        logger.info("=====> 【下级平台|发送】指令 = {} ， 数据 = {}",Integer.toHexString(basePackage.getMsgId()), ByteBufUtil.hexDump(sendToMsg));
        out.writeBytes(sendToMsg);
    }
}
