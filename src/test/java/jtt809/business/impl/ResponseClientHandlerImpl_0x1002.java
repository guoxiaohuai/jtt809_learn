package jtt809.business.impl;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import io.netty.channel.ChannelHandlerContext;
import jtt809.business.IClientBusinessServer;
import jtt809.pojo.ResponseClientJtt809_0x1002;

/**
 * 从链路连接保持请求消息
 * 链路类型:从链路。
 * 消息方向:上级平台往下级平台。
 * 业务数据类型标识:DOWN_ LINKTEST_ REQ。
 * 描述:从链路建立成功后，上级平台向下级平台发送从链路连接保持请求消息，以保持
 * 从链路的连接状态。
 * 从链路连接保持请求消息，数据体为空。
 */
public class ResponseClientHandlerImpl_0x1002 implements IClientBusinessServer<ResponseClientJtt809_0x1002> {

    private static final Log logger = LogFactory.get();

    public void businessHandler(ChannelHandlerContext ctx, ResponseClientJtt809_0x1002 msg) {
        if (msg.getResult() == 0) {

            // 将校验码变量转换成常量
            msg.setResultVerifyCode(msg.getVerifyCode());
            // 设置登录状态
            msg.setIsLoginFlagFromUpPlatform(true);

            logger.info("=====================> 登录成功！");
            logger.info("=====================> 校验码 = " + msg.getVerifyCode());
        } else {
            // 设置登录状态
            msg.setIsLoginFlagFromUpPlatform(false);
            logger.info("=====================> 登录失败，错误码：0x0" + Integer.toHexString(msg.getResult()));
        }
    }
}
