package jtt809.pojo;

import com.sjx.jtt809.pojo.BasePackage;
import com.sjx.jtt809.util.ConstantJtt809Util;
import io.netty.buffer.ByteBuf;

/**
 * 从链路连接保持应答消息
 * 链路类型：从链路:
 * 消息方向:上级平台往下级平台。
 * 业务数据类型标识:DOWN_ LINKTEST_ REP。
 * 描述:下级平台收到上级平台链路连接保持请求消息后，向上级平台返回从链路连接保
 * 持应答消息，保持从链路连接状态。
 * 从链路连接保持应答消息，数据体为空。
 */
public class RequestClientJtt809_0x9006 extends BasePackage {

    public RequestClientJtt809_0x9006() {
        super(ConstantJtt809Util.DOWN_LINKTEST_RSP);
        this.msgBodyLength = 0;
    }

    @Override
    protected void encodeImpl(ByteBuf buf) {

    }
}
